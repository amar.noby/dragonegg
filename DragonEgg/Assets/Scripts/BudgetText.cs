using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BudgetText : MonoBehaviour
{
    TextMeshProUGUI budgetText;
    // Start is called before the first frame update
    void Start()
    {
        budgetText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        budgetText.text = GameManager.coins.ToString();
    }
}
