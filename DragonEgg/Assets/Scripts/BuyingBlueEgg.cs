using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class BuyingBlueEgg : MonoBehaviour
{
    [HideInInspector] public static int price;
    TextMeshProUGUI priceText;
    public static int bought = 0;
    // Start is called before the first frame update
    void Start()
    {
        priceText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        bought = PlayerPrefs.GetInt("BlueBought");
        if (bought == 0)
        {
            price = PlayerPrefs.GetInt("BluePrice");
        }
        priceText.text = price.ToString();

    }
    public void ChangePrice()
    {
        if (bought == 0)
        {
            price = 100;
            PlayerPrefs.SetInt("BluePrice", price);
            PlayerPrefs.SetInt("BlueBought", bought);
        }
    }
}
