using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CleanBarBehaviour : MonoBehaviour
{
    public float speed;
    Slider slider;
    public GameObject towel;
    BoxCollider2D towelCollider;
    public GameObject DragonDirty;
    public Transform[] dirt = new Transform[2];
    bool clean;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        towelCollider = towel.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (clean)
        {
            slider.value += speed * Time.deltaTime;
        }
        if (slider.value > 0.5)
        {
            DragonDirty.SetActive(false);
        }
    }
    public void FillBar()
    {
        if(SleepBarBehaviour.sleeping == false)
        {
            towel.SetActive(true);
            towel.transform.position = Input.mousePosition;
            if (towelCollider.OverlapPoint(dirt[0].position) || towelCollider.OverlapPoint(dirt[1].position))
            {
                clean = true;
            }
            else
            {
                clean = false;
            }
        }
    }
    public void StopCleaning()
    {
        towel.SetActive(false);
        clean = false;
    }
}
