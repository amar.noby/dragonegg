using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EggChoice : MonoBehaviour
{
    private void Start()
    {
        SleepBarBehaviour.sleeping = false;
    }
    public void RedEgg()
    {
        StartCoroutine(SwitchToRedEgg());
    }
    public void GreenEgg()
    {
        StartCoroutine(SwitchToGreenEgg());
    }
    public void BlueEgg()
    {
        StartCoroutine(SwitchToBlueEgg());
    }
    IEnumerator SwitchToRedEgg()
    {
        if (BuyingRedEgg.price <= GameManager.coins)
        {
            GameManager.coins -= BuyingRedEgg.price;
            BuyingRedEgg.price = 0;
            BuyingRedEgg.bought = 1;
            PlayerPrefs.SetInt("RedBought", BuyingRedEgg.bought);
            yield return new WaitForSeconds(0.1f);
            SceneManager.LoadScene("RedDragon");
        }
    }
    IEnumerator SwitchToGreenEgg()
    {
        if (BuyingGreenEgg.price <= GameManager.coins)
        {
            GameManager.coins -= BuyingGreenEgg.price;
            BuyingGreenEgg.price = 0;
            BuyingGreenEgg.bought = 1;
            PlayerPrefs.SetInt("GreenBought", BuyingGreenEgg.bought);
            yield return new WaitForSeconds(0.1f);
            SceneManager.LoadScene("GreenDragon");
        }
    }
    IEnumerator SwitchToBlueEgg()
    {
        if (BuyingBlueEgg.price <= GameManager.coins)
        {
            GameManager.coins -= BuyingBlueEgg.price;
            BuyingBlueEgg.price = 0;
            BuyingBlueEgg.bought = 1;
            PlayerPrefs.SetInt("BlueBought", BuyingBlueEgg.bought);
            yield return new WaitForSeconds(0.1f);
            SceneManager.LoadScene("BlueDragon");
        }
    }

}
