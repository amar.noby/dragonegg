using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggPriceImageActivation : MonoBehaviour
{
    public GameObject[] priceImages = new GameObject[3];
    public static int[] isActiveInt = new int[3];

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        if (priceImages[0].activeInHierarchy && priceImages[1].activeInHierarchy)
        {
            isActiveInt[0] = 1;
            isActiveInt[1] = 1;
            PlayerPrefs.SetInt("Activation 0", isActiveInt[0]);
            PlayerPrefs.SetInt("Activation 1", isActiveInt[1]);
        }
        if (priceImages[0].activeInHierarchy && priceImages[2].activeInHierarchy)
        {
            isActiveInt[0] = 1;
            isActiveInt[2] = 1;
            PlayerPrefs.SetInt("Activation 0", isActiveInt[0]);
            PlayerPrefs.SetInt("Activation 2", isActiveInt[2]);
        }
        if (priceImages[2].activeInHierarchy && priceImages[1].activeInHierarchy)
        {
            isActiveInt[2] = 1;
            isActiveInt[1] = 1;
            PlayerPrefs.SetInt("Activation 2", isActiveInt[2]);
            PlayerPrefs.SetInt("Activation 1", isActiveInt[1]);
        }

        if (PlayerPrefs.GetInt("Activation 0") == 1 && PlayerPrefs.GetInt("Activation 1") == 1)
        {
            priceImages[0].SetActive(true);
            priceImages[1].SetActive(true);
        }
        if (PlayerPrefs.GetInt("Activation 0") == 1 && PlayerPrefs.GetInt("Activation 2") == 1)
        {
            priceImages[0].SetActive(true);
            priceImages[2].SetActive(true);
        }
        if (PlayerPrefs.GetInt("Activation 2") == 1 && PlayerPrefs.GetInt("Activation 1") == 1)
        {
            priceImages[2].SetActive(true);
            priceImages[1].SetActive(true);
        }

    }
}
