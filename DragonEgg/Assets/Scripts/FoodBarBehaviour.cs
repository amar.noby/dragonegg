using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodBarBehaviour : MonoBehaviour
{
    public float speed;
    Slider slider;
    public float increaseFraction;
    public GameObject sandwich;
    BoxCollider2D sandwichCollider;
    public Transform mouth;
    bool fed;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        sandwichCollider = sandwich.GetComponent<BoxCollider2D>();
        audioSource = GetComponent<AudioSource>();
    }
    void Update()
    {
        if (fed == true)
        {
            slider.value += increaseFraction;
            fed = false;
        }
    }
    public void FillBar()
    {
        if (SleepBarBehaviour.sleeping == false)
        {
            sandwich.SetActive(true);
            sandwich.transform.position = Input.mousePosition;
            if (sandwichCollider.OverlapPoint(mouth.position))
            {
                fed = true;
                sandwich.SetActive(false);
                audioSource.Play();
            }
        }
    }
    public void StopFeeding()
    {
        sandwich.SetActive(false);
    }
}
