using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public Slider loveSlider;
    public Slider cleanSlider;
    public Slider foodSlider;
    public Slider playSlider;
    public Slider sleepSlider;
    public GameObject DragonNeutral;
    public GameObject DragonSad;
    public GameObject DragonDirty;
    public GameObject DragonAsleep;
    public GameObject DragonEating;
    public TextMeshProUGUI budget;
    public GameObject sandwich;
    public static int coins;
    bool coinsBool = true;
    // Start is called before the first frame update
  
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        budget.text = coins.ToString();
        // If the dragon's eating, show the open mouth.
        if (cleanSlider.value > 0.5 && sandwich.activeInHierarchy)
        {
            DragonNeutral.SetActive(false);
            DragonEating.SetActive(true);
            DragonSad.SetActive(false);
        }
        // If the dragon's not eating and the stats are below average, show the sad dragon.
        else if (!DragonDirty.activeInHierarchy && !sandwich.activeInHierarchy && (foodSlider.value < 0.5 || loveSlider.value < 0.5 || playSlider.value < 0.5 ||sleepSlider.value < 0.5))
        {
            DragonEating.SetActive(false);
            DragonSad.SetActive(true);
        }
        // If the dragon is not clean, show the dirty dragon.
        if (cleanSlider.value < 0.5)
        {
            DragonDirty.SetActive(true);
        }
        // If all stats are full, get 10 coins.
        if (loveSlider.value == loveSlider.maxValue && foodSlider.value == foodSlider.maxValue && playSlider.value == playSlider.maxValue && sleepSlider.value == sleepSlider.maxValue)
        {
            if (coinsBool)
            {
                coins += 10;
                coinsBool = false;
            }
        }
        // If the dragon is sleeping, don't let any other dragon appear.
        if (DragonAsleep.activeInHierarchy)
        {
            DragonNeutral.SetActive(false);
            DragonEating.SetActive(false);
            DragonSad.SetActive(false);
        }
        // If all stats are above average and the dragon is awake, show neutral dragon.
        if(foodSlider.value > 0.5 && loveSlider.value > 0.5 && playSlider.value > 0.5 && sleepSlider.value > 0.5 && cleanSlider.value > 0.5 && !DragonAsleep.activeInHierarchy)
        {
            DragonNeutral.SetActive(true);
            DragonSad.SetActive(false);

        }
    }
    
}
