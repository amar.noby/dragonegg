using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCoins : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.coins = PlayerPrefs.GetInt("CoinCount");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnApplicationQuit()
    {
        // Set the value of the key "CoinCount" to the coin count variable
        PlayerPrefs.SetInt("CoinCount", GameManager.coins);
    }
}
