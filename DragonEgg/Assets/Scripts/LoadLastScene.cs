using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class LoadLastScene : MonoBehaviour
{
    //int lastSceneIndex;
    int currentSceneIndex;
    int comparingSceneIndex;

    public List<int> sceneList;

    private void Update()
    {
        comparingSceneIndex = SceneManager.GetActiveScene().buildIndex;

        for (int i = 0; i < sceneList.Count; i++)
        {
            if (sceneList[i] == currentSceneIndex)
            {
                sceneList.Remove(sceneList.Last());
            }
        }
        if (comparingSceneIndex != currentSceneIndex)
        {
            sceneList.Add(currentSceneIndex);
            currentSceneIndex = comparingSceneIndex;
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(sceneList.Count > 0)
            {
                SceneManager.LoadScene(sceneList.Last());
            }
            else
            {
                Application.Quit();
            }

        }
    }
}
