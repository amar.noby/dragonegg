using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoveBarBehaviour : MonoBehaviour
{
    public float speed;
    Slider slider;
    public GameObject love;
    bool loving;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (loving == true)
        {
            slider.value += speed * Time.deltaTime;
        }
    }
    public void FillBar()
    {
        if (SleepBarBehaviour.sleeping == false)
        {
            love.SetActive(true);
            loving = true;
            audioSource.Play();
        }
    }
    public void StopLoving()
    {
        love.SetActive(false);
        loving = false;
        audioSource.Stop();
    }
}
