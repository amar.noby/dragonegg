using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuRussian : MonoBehaviour
{
    public void StartGame()
    {
        StartCoroutine(SwitchToEggChoice());
    }
    public void SettingsScreen()
    {
        StartCoroutine(SwitchToSettings());
    }
    IEnumerator SwitchToEggChoice()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("EggChoiceRussian");
    }
    IEnumerator SwitchToSettings()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("SettingsRussian");
    }
}
