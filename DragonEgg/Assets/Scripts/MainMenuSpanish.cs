using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuSpanish : MonoBehaviour
{
    public void StartGame()
    {
        StartCoroutine(SwitchToEggChoice());
    }
    public void SettingsScreen()
    {
        StartCoroutine(SwitchToSettings());
    }
    IEnumerator SwitchToEggChoice()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("EggChoiceSpanish");
    }
    IEnumerator SwitchToSettings()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("SettingsSpanish");
    }
}
