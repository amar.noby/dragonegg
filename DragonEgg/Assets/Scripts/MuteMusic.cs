using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteMusic : MonoBehaviour
{
    AudioSource music;

    // Start is called before the first frame update
    void Start()
    {
        music = BackgroundMusic.backgroundMusic.GetComponent<AudioSource>();
    }

    public void MuteBackgroundMusic()
    {
        music.mute = true;
    }
    public void UnMuteMusic()
    {
        music.mute = false;
    }
}
