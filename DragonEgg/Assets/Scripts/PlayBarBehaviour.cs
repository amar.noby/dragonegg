using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayBarBehaviour : MonoBehaviour
{
    public float speed;
    Slider slider;
    public GameObject ball;
    static public bool playing;
    public Transform ballStartPosition;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        rb = ball.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playing == true)
        {
            slider.value += speed * Time.deltaTime;
        }
    }
    public void FillBar()
    {
        if(SleepBarBehaviour.sleeping == false)
        {
            ball.SetActive(true);
            ball.transform.position = Input.mousePosition;
            playing = true;
        }
    }
    public void StopPlaying()
    {
        ball.SetActive(false);
        playing = false;
    }
}
