using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsEnglish : MonoBehaviour
{
    IEnumerator SwitchScene()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("MainMenuRussian");
    }
    public void EnglishToRussian()
    {
        StartCoroutine(SwitchScene());
    }
}
