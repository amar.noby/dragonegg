using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsRussian : MonoBehaviour
{
    IEnumerator SwitchScene()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("MainMenuSpanish");
    }
    public void RussianToSpanish()
    {
        StartCoroutine(SwitchScene());
    }
}
