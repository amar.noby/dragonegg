using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingsSpanish : MonoBehaviour
{
    IEnumerator SwitchScene()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("MainMenuEnglish");
    }
    public void SpanishToEnglish()
    {
        StartCoroutine(SwitchScene());
    }
}
