using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SleepBarBehaviour : MonoBehaviour
{
    public float speed;
    Slider slider;
    public GameObject DragonAsleep;
    static public bool sleeping = false;
    public Slider cleanSlider;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (sleeping == true)
        {
            slider.value += speed * Time.deltaTime;
        }
    }
    public void FillBar()
    {
        if(PlayBarBehaviour.playing == false && cleanSlider.value > 0.5f && sleeping == false)
        {
            DragonAsleep.SetActive(true);
            sleeping = true;
            audioSource.Play();
        }
    }
    public void StopSleeping()
    {
        DragonAsleep.SetActive(false);
        sleeping = false;
        audioSource.Stop();
    }
}
